@php $lang_local = app()->getLocale() @endphp

@if($booking->start_date)
    <tr>
        <td class="label">{{__('Start date:')}}</td>
        <td class="val">
            {{display_date($booking->start_date)}}
        </td>
    </tr>
    <tr>
        <td class="label">{{__('Duration:')}}</td>
        <td class="val">
            {{human_time_diff($booking->end_date,$booking->start_date)}}
        </td>
    </tr>
@endif
@php $person_types = $booking->getJsonMeta('person_types')@endphp
@if(!empty($person_types))
    @foreach($person_types as $type)
        <tr>
            <td class="label">{{$type['name_'.$lang_local] ?? $type['name']}}:</td>
            <td class="val">
                {{$type['number']}}
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td class="label">{{__("Guests")}}:</td>
        <td class="val">
            {{$booking->total_guests}}
        </td>
    </tr>
@endif

</ul>
</td>

@php $person_types = $booking->getJsonMeta('person_types') @endphp
@if(!empty($person_types))
    @foreach($person_types as $type)
        <tr>
            <td class="label">{{ $type['name_'.$lang_local] ?? $type['name']}}: {{$type['number']}} * {{format_money($type['price'])}}</td>
            <td class="val">
                {{format_money($type['price'] * $type['number'])}}
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td class="label">{{__("Guests")}}: {{$booking->total_guests}} * {{format_money($booking->getMeta('base_price'))}}</td>
        <td class="val">
            {{format_money($booking->getMeta('base_price') * $booking->total_guests)}}
        </td>
    </tr>
@endif
@php $extra_price = $booking->getJsonMeta('extra_price') @endphp
@if(!empty($extra_price))
    <tr>
        <td class="label-title"><strong>{{__("Extra Prices:")}}</strong></td>
    </tr>
    <tr class="no-flex">
        <ul>
            @foreach($extra_price as $type)
                <tr>
                    <td class="label">{{$type['name_'.$lang_local] ?? $type['name']}}:</td>
                    <td class="val">
                        {{format_money($type['total'] ?? 0)}}
                    </td>
                </tr>
            @endforeach
        </ul>
    </tr>
@endif
@php $discount_by_people = $booking->getJsonMeta('discount_by_people')@endphp
@if(!empty($discount_by_people))
    <tr>
        <td class="label-title"><strong>{{__("Discounts:")}}</strong></td>
    </tr>
    <tr class="no-flex">
        <ul>
            @foreach($discount_by_people as $type)
                <tr>
                    <td class="label">
                        @if(!$type['to'])
                            {{__('from :from guests',['from'=>$type['from']])}}
                        @else
                            {{__(':from - :to guests',['from'=>$type['from'],'to'=>$type['to']])}}
                        @endif
                        :
                    </td>
                    <td class="val">
                        - {{format_money($type['total'] ?? 0)}}
                    </td>
                </tr>
            @endforeach
        </ul>
    </tr>
@endif
@if(!empty($booking->buyer_fees))
    <?php
    $buyer_fees = json_decode($booking->buyer_fees , true);
    foreach ($buyer_fees as $buyer_fee){
    ?>
    <tr>
        <td class="label">
            {{$buyer_fee['name_'.$lang_local] ?? $buyer_fee['name']}}
            <i class="icofont-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $buyer_fee['desc_'.$lang_local] ?? $buyer_fee['desc'] }}"></i>
            @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                : {{$booking->total_guests}} * {{format_money( $buyer_fee['price'] )}}
            @endif
        </td>
        <td class="val">
            @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                {{ format_money( $buyer_fee['price'] * $booking->total_guests ) }}
            @else
                {{ format_money($buyer_fee['price']) }}
            @endif
        </td>
    </tr>
    <?php } ?>
@endif
<tr class="final-total">
    <td class="label">{{__("Total:")}}</td>
    <td class="val">{{format_money($booking->total)}}</td>
</tr>
    