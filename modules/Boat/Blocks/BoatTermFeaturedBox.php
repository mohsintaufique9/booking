<?php
namespace Modules\Boat\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Modules\Core\Models\Terms;

class BoatTermFeaturedBox extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Title')
                ],
                [
                    'id'        => 'desc',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Desc')
                ],
                [
                    'id'           => 'term_boat',
                    'type'         => 'select2',
                    'label'        => __('Select term boat'),
                    'select2'      => [
                        'ajax'     => [
                            'url'      => route('boat.admin.attribute.term.getForSelect2', ['type' => 'boat']),
                            'dataType' => 'json'
                        ],
                        'width'    => '100%',
                        'multiple' => "true",
                    ],
                    'pre_selected' => route('boat.admin.attribute.term.getForSelect2', [
                        'type'         => 'space',
                        'pre_selected' => 1
                    ])
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('Boat: Term Featured Box');
    }

    public function content($model = [])
    {
        if (empty($term_space = $model['term_boat'])) {
            return "";
        }
        $list_term = Terms::whereIn('id',$term_space)->get();
        $model['list_term'] = $list_term;
        return view('Boat::frontend.blocks.term-featured-box.index', $model);
    }
}