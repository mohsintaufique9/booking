@php $lang_local = app()->getLocale() @endphp
<div class="booking-review">
    <h4 class="booking-review-title">{{__("Your Booking")}}</h4>
    <div class="booking-review-content">
        <div class="review-section">
            <div class="service-info">
                <div>
                    @php
                        $service_translation = $service->translateOrOrigin($lang_local);
                    @endphp
                    <h3 class="service-name"><a href="{{$service->getDetailUrl()}}">{{$service_translation->title}}</a></h3>
                    <div class="row mt-4">
                        <div class="col-5">
                            @if($booking->service->dLocation)
                                <p class="text-primary">{{ $booking->service->dLocation->name}}</p>
                                <p class="text-booking">
                                    {{ \Carbon\Carbon::parse($booking->service->departure_time)->format('H:i') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-2 p-3 text-center">
                            <i class="fa fa-2x fa-ship" aria-hidden="true"></i>
                        </div>
                         <div class="col-5 text-right">
                            @if($booking->service->arrival_time)
                                <p class="text-primary">{{ $booking->service->aLocation->name}}</p>
                                <p class="text-booking">
                                    {{ \Carbon\Carbon::parse($booking->service->arrival_time)->format('H:i') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="row pl-5">
                        <h6 class="text-dark pl-2 text-center"> Estimated Time: {{ $service->getTravelTime() }}</h6>
                    </div>
                </div>
                <div>
                </div>
            </div>
        </div>
        <div class="review-section">
            <ul class="review-list">
                @if($booking->start_date)
                    <li>
                        <div class="label">{{__('Booking date:')}}</div>
                        <div class="val">
                            {{display_date($booking->start_date)}}
                        </div>
                    </li>
                @endif

                @if($booking->fc_number)
                    <li>
                        <div class="label">{{__('Number Of Tickets:')}}</div>
                        <div class="val">
                            {{ $booking->fc_number }}
                        </div>
                    </li>
                @endif

                @if($booking->sc_number)
                    <li>
                        <div class="label">{{__('Number Of Second Cls. Tickets:')}}</div>
                        <div class="val">
                            {{ $booking->sc_number }}
                        </div>
                    </li>
                @endif
                @if($booking->bc_number)
                    <li>
                        <div class="label">{{__('Number Of Business Cls. Tickets:')}}</div>
                        <div class="val">
                            {{ $booking->bc_number }}
                        </div>
                    </li>
                @endif
            </ul>
        </div>
        {{--@include('Booking::frontend/booking/checkout-coupon')--}}
        <div class="review-section total-review">
            <ul class="review-list">
                <li>
                    @if($booking->fc_number)
                        <div class="label"> {{ format_money($booking->service->getfcPrice())}} *
                           {{  $booking->fc_number }}
                        </div>
                        <div class="val">
                            {{ format_money($booking->fcTotal()) }}
                        </div>
                     @endif
                </li>
                <li>
                    @if($booking->sc_number)
                        <div class="label"> Second Cls. {{ format_money($booking->service->getscPrice())}} *
                            {{  $booking->sc_number }}
                        </div>
                        <div class="val">
                            {{ format_money($booking->scTotal()) }}
                        </div>
                    @endif
                </li>
                 <li>
                    @if($booking->bc_number)
                        <div class="label"> Business Cls. {{ format_money($booking->service->getbcPrice())}} *
                            {{  $booking->bc_number }}
                        </div>
                        <div class="val">
                            {{ format_money($booking->bcTotal()) }}
                        </div>
                    @endif
                </li>
                @php $extra_price = $booking->getJsonMeta('extra_price') @endphp
                @if(!empty($extra_price))
                    <li>
                        <div class="label-title"><strong>{{__("Extra Prices:")}}</strong></div>
                    </li>
                    <li class="no-flex">
                        <ul>
                            @foreach($extra_price as $type)
                                <li>
                                    <div class="label">{{$type['name_'.$lang_local] ?? $type['name']}}:</div>
                                    <div class="val">
                                        {{format_money($type['total'] ?? 0)}}
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                @if(!empty($booking->buyer_fees))
                    <?php
                    $buyer_fees = json_decode($booking->buyer_fees , true);
                    foreach ($buyer_fees as $buyer_fee){
                        ?>
                        <li>
                            <div class="label">
                                {{$buyer_fee['name_'.$lang_local] ?? $buyer_fee['name']}}
                                <i class="icofont-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $buyer_fee['desc_'.$lang_local] ?? $buyer_fee['desc'] }}"></i>
                                @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                                    : {{$booking->total_guests}} * {{format_money( $buyer_fee['price'] )}}
                                @endif
                            </div>
                            <div class="val">
                                @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                                    {{ format_money( $buyer_fee['price'] * $booking->total_guests ) }}
                                @else
                                {{ format_money($buyer_fee['price']) }}
                                @endif
                            </div>
                        </li>
                    <?php } ?>
                @endif
                <li class="final-total">
                    <div class="label">{{__("Total:")}}</div>
                    <div class="val">{{format_money($booking->total)}}</div>
                </li>
            </ul>
        </div>
    </div>
</div>