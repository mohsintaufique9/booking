<?php
use \Illuminate\Support\Facades\Route;

Route::group(['prefix'=>config('boat.boat_route_prefix')],function(){
    Route::get('/','BoatController@index')->name('boat.search'); // Search
    Route::get('/{slug}','BoatController@detail')->name('boat.detail');// Detail
    Route::get('/booking/{slug}','BoatController@booking')->name('boat.booking');// Booking
    Route::post('/booking/add-to-cart/{id}','BoatController@addToCart')->name('boat.add-to-cart');// cart
    
});

Route::group(['prefix'=>'user/'.config('boat.boat_route_prefix')],function(){

    Route::match(['get','post'],'/','ManageBoatController@manageBoat')->name('boat.vendor.index');
    Route::match(['get','post'],'/create','ManageBoatController@createBoat')->name('boat.vendor.create');
    Route::match(['get','post'],'/edit/{slug}','ManageBoatController@editBoat')->name('boat.vendor.edit');
    Route::match(['get','post'],'/del/{slug}','ManageBoatController@deleteBoat')->name('boat.vendor.delete');
    Route::match(['post'],'/store/{slug}','ManageBoatController@store')->name('boat.vendor.store');
    Route::get('bulkEdit/{id}','ManageBoatController@bulkEditBoat')->name("boat.vendor.bulk_edit");
    Route::get('/booking-report','ManageBoatController@bookingReport')->name("boat.vendor.booking_report");
    Route::get('/booking-report/bulkEdit/{id}','ManageBoatController@bookingReportBulkEdit')->name("boat.vendor.booking_report.bulk_edit");

    Route::group(['prefix'=>'availability'],function(){
        Route::get('/','AvailabilityController@index')->name('boat.vendor.availability.index');
        Route::get('/loadDates','AvailabilityController@loadDates')->name('boat.vendor.availability.loadDates');
        Route::match(['get','post'],'/store','AvailabilityController@store')->name('boat.vendor.availability.store');
    });
});