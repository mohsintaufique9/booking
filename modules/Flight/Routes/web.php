<?php
use \Illuminate\Support\Facades\Route;

Route::group(['prefix'=>config('flight.flight_route_prefix')],function(){
    Route::get('/','FlightController@index')->name('flight.search'); // Search
    Route::get('/{slug}','FlightController@detail')->name('flight.detail');// Detail
    Route::get('/booking/{slug}','FlightController@booking')->name('flight.booking');// Booking
    Route::post('/booking/add-to-cart/{id}','FlightController@addToCart')->name('flight.add-to-cart');// cart
});

Route::group(['prefix'=>'user/'.config('flight.flight_route_prefix')],function(){

    Route::match(['get','post'],'/','ManageFlightController@manageFlight')->name('flight.vendor.index');
    Route::match(['get','post'],'/create','ManageFlightController@createFlight')->name('flight.vendor.create');
    Route::match(['get','post'],'/edit/{slug}','ManageFlightController@editFlight')->name('flight.vendor.edit');
    Route::match(['get','post'],'/del/{slug}','ManageFlightController@deleteFlight')->name('flight.vendor.delete');
    Route::match(['post'],'/store/{slug}','ManageFlightController@store')->name('flight.vendor.store');
    Route::get('bulkEdit/{id}','ManageFlightController@bulkEditFlight')->name("flight.vendor.bulk_edit");
    Route::get('/booking-report','ManageFlightController@bookingReport')->name("flight.vendor.booking_report");
    Route::get('/booking-report/bulkEdit/{id}','ManageFlightController@bookingReportBulkEdit')->name("flight.vendor.booking_report.bulk_edit");

    Route::group(['prefix'=>'availability'],function(){
        Route::get('/','AvailabilityController@index')->name('flight.vendor.availability.index');
        Route::get('/loadDates','AvailabilityController@loadDates')->name('flight.vendor.availability.loadDates');
        Route::match(['get','post'],'/store','AvailabilityController@store')->name('flight.vendor.availability.store');
    });
});