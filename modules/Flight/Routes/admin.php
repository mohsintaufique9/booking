<?php

use \Illuminate\Support\Facades\Route;


Route::get('/','FlightController@index')->name('flight.admin.index');
Route::get('/create','FlightController@create')->name('flight.admin.create');
Route::get('/edit/{id}','FlightController@edit')->name('flight.admin.edit');
Route::post('/store/{id}','FlightController@store')->name('flight.admin.store');
Route::post('/bulkEdit','FlightController@bulkEdit')->name('flight.admin.bulkEdit');
Route::post('/bulkEdit','FlightController@bulkEdit')->name('flight.admin.bulkEdit');

Route::group(['prefix'=>'attribute'],function (){
    Route::get('/','AttributeController@index')->name('flight.admin.attribute.index');
    Route::get('edit/{id}','AttributeController@edit')->name('flight.admin.attribute.edit');
    Route::post('store/{id}','AttributeController@store')->name('flight.admin.attribute.store');

    Route::get('terms/{id}','AttributeController@terms')->name('flight.admin.attribute.term.index');
    Route::get('term_edit/{id}','AttributeController@term_edit')->name('flight.admin.attribute.term.edit');
    Route::get('term_store','AttributeController@term_store')->name('flight.admin.attribute.term.store');

    Route::get('getForSelect2','AttributeController@getForSelect2')->name('flight.admin.attribute.term.getForSelect2');
});

Route::group(['prefix'=>'availability'],function(){
    Route::get('/','AvailabilityController@index')->name('flight.admin.availability.index');
    Route::get('/loadDates','AvailabilityController@loadDates')->name('flight.admin.availability.loadDates');
    Route::match(['get','post'],'/store','AvailabilityController@store')->name('flight.admin.availability.store');
});
