<style>
.class-list li:hover {
  color: #5191FA;
}
.active {
    color: #5191FA;
}
.class-list {
    padding : 20px;
}
.class-list li {
    padding : 5px;
}
</style>

<div class="bravo_single_book_wrap">
    <div class="bravo_single_book">
        <div id="bravo_space_book_app" v-cloak>
            @if($row->discount_percent)
                <div class="tour-sale-box">
                    <span class="sale_class box_sale sale_small">{{$row->discount_percent}}</span>
                </div>
            @endif
            <div class="form-head">
                <div class="row">
                    <div class="col-md-4 pl-5">
                        <div class="location p-2 ">
                            @if(!empty($row->departure_time))
                                <h6>{{ \Carbon\Carbon::parse($row->departure_time)->format('H:i') }}</h6>
                            @endif
                        </div>
                        <div class="location p-2">
                            @if(!empty($row->arrival_time))
                                <h6>{{ \Carbon\Carbon::parse($row->arrival_time)->format('H:i') }}</h6>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2 pt-3 text-right">
                        <i class="fa fa-2x fa-plane"></i>
                    </div>
                    <div class="col-md-6 float-right">
                        <div class="location p-2" >
                            @if(!empty($row->dLocation->name))
                                <h6 class="text-primary">{{ $row->dLocation->name }}</h6>
                            @endif
                        </div>
                        <div class="location p-2">
                            @if(!empty($row->aLocation->name))
                                <h6 class="text-primary">{{ $row->aLocation->name }}</h6>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-content">
                <div class="row">
                    <div class="col-md-6">
                        @if(Request::input('start'))
                            <div class="pl-3 pr-3 pt-3 d-flex">
                                <div class="flex-grow-1">
                                    <label class="text-secondary">{{__("Departure Date:")}}</label>
                                    <span class="text-primary"> {{ \Carbon\Carbon::parse(Request::input('start'))->format('d/m/Y') }}</span>
                                </div>
                            </div>
                        @endif
                    </div>

                    
                    @if(Request::input('end'))
                        <div class="col-md-6">
                            <div class="pl-3 pr-3 pt-3 d-flex">
                                <div class="flex-grow-1">
                                    <label class="text-secondary">{{__("Arrival Date:")}}</label>
                                    <span class="text-primary"> {{ \Carbon\Carbon::parse(Request::input('start'))->format('d/m/Y') }}</span>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(Request::input('class'))
                        <div class="col-md-6">
                            <div class="pl-3 pr-3 pt-3 d-flex">
                                <div class="flex-grow-1">
                                    <label class="text-secondary">{{__("Selected Class:")}}</label>
                                    <span class="text-primary"> {{ Request::input('class') }}</span>
                                </div>
                            </div>
                        </div>
                    @endif
                    
                </div>
                <input type="text" class="start_date" ref="start_date" style="height: 1px; visibility: hidden" value="{{ Request::input('start') }}">
                <input type="text" class="start_date" ref="end_date" style="height: 1px; visibility: hidden" value="{{ Request::input('end') }}">

                <div class="form-group">
                    <input type="hidden" name="class_id" class="class_id" value="{{ Request::input('class') }}">
                     {{-- <div class="dropdown-item-row">
                        <ul class="list-unstyled class-list">
                            <li data-id="1" class="item active"><i class="fa pull-right icon d-none fa-check"></i>{{__('Economy')}}</li>
                            <li data-id="2" class="item"><i class="fa pull-right icon d-none fa-check"></i>{{__('Ecn. Premium')}}</li>
                            <li data-id="3" class="item"><i class="fa pull-right icon d-none fa-check"></i>{{__('First Class')}}</li>
                            <li data-id="4" class="item"><i class="fa pull-right icon d-none fa-check"></i>{{__('Business Class')}}</li>
                        </ul>
                    </div> --}}
                </div>

                <div class="form-group form-guest-search">
                    <div class="guest-wrapper d-flex justify-content-between align-items-center">
                        <div class="flex-grow-1">
                            <label>{{__("Adult Tickets")}}</label>
                        </div>
                        <div class="flex-shrink-0">
                            <div class="input-number-group">
                                <i class="icon ion-ios-remove-circle-outline" @click="minusAdult()"></i>
                                <span class="input">@{{ adults }}</span>
                                <i class="icon ion-ios-add-circle-outline" @click="addAdult()"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group form-guest-search">
                    <div class="guest-wrapper d-flex justify-content-between align-items-center">
                        <div class="flex-grow-1">
                            <label>{{__("Child Tickets")}}</label>
                        </div>
                        <div class="flex-shrink-0">
                            <div class="input-number-group">
                                <i class="icon ion-ios-remove-circle-outline" @click="minusChild()"></i>
                                <span class="input">@{{ child }}</span>
                                <i class="icon ion-ios-add-circle-outline" @click="addChild()"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-section-group form-group" v-if="extra_price.length">
                    <h4 class="form-section-title">{{__('Extra prices:')}}</h4>
                    <div class="form-group " v-for="(type,index) in extra_price">
                        <div class="extra-price-wrap d-flex justify-content-between">
                            <div class="flex-grow-1">
                                <label><input type="checkbox" v-model="type.enable"> @{{type.name}}</label>
                                <div class="render" v-if="type.price_type">(@{{type.price_type}})</div>
                            </div>
                            <div class="flex-shrink-0">@{{type.price_html}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-section-group form-group-padding" v-if="buyer_fees.length">
                    <div class="extra-price-wrap d-flex justify-content-between" v-for="(type,index) in buyer_fees">
                        <div class="flex-grow-1">
                            <label>@{{type.type_name}}
                                <i class="icofont-info-circle" v-if="type.desc" data-toggle="tooltip" data-placement="top" :title="type.type_desc"></i>
                            </label>
                            <div class="render" v-if="type.price_type">(@{{type.price_type}})</div>
                        </div>
                        <div class="flex-shrink-0">@{{formatMoney(type.price)}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-section-total" v-if="total_price > 0">
                <label>{{__("Total")}}</label>
                <span class="price">@{{total_price_html}}</span>
            </div>
            <div v-html="html"></div>
            <div class="submit-group">
                <p><i>
                        @if($row->max_guests <= 1)
                            {{__(':count Guest in maximum',['count'=>$row->max_guests])}}
                        @else
                            {{__(':count Guests in maximum',['count'=>$row->max_guests])}}
                        @endif
                    </i>
                </p>
                <a class="btn btn-large" @click="doSubmit($event)" :class="{'disabled':onSubmit,'btn-success':(step == 2),'btn-primary':step == 1}" name="submit">
                    <span v-if="step == 1">{{__("BOOK NOW")}}</span>
                    <span v-if="step == 2">{{__("Book Now")}}</span>
                    <i v-show="onSubmit" class="fa fa-spinner fa-spin"></i>
                </a>
                <div class="alert-text mt10" v-show="message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></div>
            </div>
        </div>
    </div>
</div>
