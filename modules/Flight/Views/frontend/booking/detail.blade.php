@php $lang_local = app()->getLocale() @endphp
<div class="booking-review">
    <h4 class="booking-review-title">{{__("Your Booking")}}</h4>
    <div class="booking-review-content">
        <div class="review-section">
            <div class="service-info">
                <div>
                    @php
                        $service_translation = $service->translateOrOrigin($lang_local);
                    @endphp
                    <h3 class="service-name"><a href="{{$service->getDetailUrl()}}">{{$service_translation->title}}</a></h3>
                    <div class="row mt-4">
                        <div class="col-md-5">
                            @if($booking->service->dLocation)
                                <p class="text-primary">{{ $booking->service->dLocation->name}}</p>
                                <p class="text-booking">
                                    {{ \Carbon\Carbon::parse($booking->service->departure_time)->format('H:i') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-md-2 p-3 text-center">
                            <i class="fa fa-2x fa-plane" aria-hidden="true"></i>
                        </div>
                         <div class="col-md-5 text-right">
                            @if(\Carbon\Carbon::parse($booking->service->arrival_time)->format('H:i'))
                                <p class="text-primary">{{ $booking->service->aLocation->name}}</p>
                                <p class="text-booking">
                                    {{ \Carbon\Carbon::parse($booking->service->arrival_time)->format('H:i') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="row pl-5">
                        <h6 class="text-dark pl-2 text-center"> Estimated Time: {{ $service->getTravelTime() }}</h6>
                    </div>
                </div>
                <div>
                </div>
            </div>
        </div>
        <div class="review-section">
            <ul class="review-list">
                @if($booking->start_date)
                    <li>
                        <div class="label">{{__('Departure Date:')}}</div>
                        <div class="val">
                            {{display_date($booking->start_date)}}
                        </div>
                    </li>
                @endif

                  @if($booking->end_date)
                    <li>
                        <div class="label">{{__('Arrival Date:')}}</div>
                        <div class="val">
                            {{display_date($booking->end_date)}}
                        </div>
                    </li>
                @endif

                @if($booking->adults)
                    <li>
                        <div class="label">{{__('Number Of Adult Tickets:')}}</div>
                        <div class="val">
                            {{ $booking->adults }}
                        </div>
                    </li>
                @endif

                @if($booking->child)
                    <li>
                        <div class="label">{{__('Number Of Children Tickets:')}}</div>
                        <div class="val">
                            {{ $booking->child }}
                        </div>
                    </li>
                @endif
            </ul>
        </div>
        {{--@include('Booking::frontend/booking/checkout-coupon')--}}
        <div class="review-section total-review">
            <ul class="review-list">
            @php 
                $adultinfo = $booking->getBookingTotal(0);
                $childinfo = $booking->getBookingTotal(1);
            @endphp
            
                <li>
                    @if($booking->adults)
                        <div class="label"> {{ $adultinfo->name }} {{ format_money($adultinfo->price)}} *
                            {{ $booking->adults }}
                        </div>
                        <div class="val">
                            {{ format_money($adultinfo->total) }}
                        </div>
                    @endif
                </li>

                <li>
                    @if($booking->child)
                        <div class="label"> {{ $childinfo->name }} {{ format_money($childinfo->price)}} *
                            {{ $booking->child }}
                        </div>
                        <div class="val">
                            {{ format_money($childinfo->total) }}
                        </div>
                    @endif
                </li>
             
                @php $extra_price = $booking->getJsonMeta('extra_price') @endphp
                @if(!empty($extra_price))
                    <li>
                        <div class="label-title"><strong>{{__("Extra Prices:")}}</strong></div>
                    </li>
                    <li class="no-flex">
                        <ul>
                            @foreach($extra_price as $type)
                                <li>
                                    <div class="label">{{$type['name_'.$lang_local] ?? $type['name']}}:</div>
                                    <div class="val">
                                        {{format_money($type['total'] ?? 0)}}
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                @if(!empty($booking->buyer_fees))
                    <?php
                    $buyer_fees = json_decode($booking->buyer_fees , true);
                    foreach ($buyer_fees as $buyer_fee){
                        ?>
                        <li>
                            <div class="label">
                                {{$buyer_fee['name_'.$lang_local] ?? $buyer_fee['name']}}
                                <i class="icofont-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $buyer_fee['desc_'.$lang_local] ?? $buyer_fee['desc'] }}"></i>
                                @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                                    : {{$booking->total_guests}} * {{format_money( $buyer_fee['price'] )}}
                                @endif
                            </div>
                            <div class="val">
                                @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                                    {{ format_money( $buyer_fee['price'] * $booking->total_guests ) }}
                                @else
                                {{ format_money($buyer_fee['price']) }}
                                @endif
                            </div>
                        </li>
                    <?php } ?>
                @endif
                <li class="final-total">
                    <div class="label">{{__("Total:")}}</div>
                    <div class="val">{{format_money($booking->total)}}</div>
                </li>
            </ul>
        </div>
    </div>
</div>