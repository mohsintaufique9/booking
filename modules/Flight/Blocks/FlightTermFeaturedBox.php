<?php
namespace Modules\Flight\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Modules\Core\Models\Terms;

class FlightTermFeaturedBox extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Title')
                ],
                [
                    'id'        => 'desc',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Desc')
                ],
                [
                    'id'           => 'term_flight',
                    'type'         => 'select2',
                    'label'        => __('Select term flight'),
                    'select2'      => [
                        'ajax'     => [
                            'url'      => route('flight.admin.attribute.term.getForSelect2', ['type' => 'flight']),
                            'dataType' => 'json'
                        ],
                        'width'    => '100%',
                        'multiple' => "true",
                    ],
                    'pre_selected' => route('flight.admin.attribute.term.getForSelect2', [
                        'type'         => 'space',
                        'pre_selected' => 1
                    ])
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('Flight: Term Featured Box');
    }

    public function content($model = [])
    {
        if (empty($term_space = $model['term_flight'])) {
            return "";
        }
        $list_term = Terms::whereIn('id',$term_space)->get();
        $model['list_term'] = $list_term;
        return view('Flight::frontend.blocks.term-featured-box.index', $model);
    }
}