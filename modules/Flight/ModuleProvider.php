<?php
namespace Modules\Flight;
use Modules\Flight\Models\Flight;
use Modules\ModuleServiceProvider;

class ModuleProvider extends ModuleServiceProvider
{

    public function boot(){

        $this->loadMigrationsFrom(__DIR__ . '/Migrations');

    }
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouterServiceProvider::class);
    }

    public static function getAdminMenu()
    {
        if(!Flight::isEnable()) return [];
        return [
            'flight'=>[
                "position"=>45,
                'url'        => 'admin/module/flight',
                'title'      => __('Flight'),
                'icon'       => 'ion-logo-model-s',
                'permission' => 'flight_view',
                'children'   => [
                    'add'=>[
                        'url'        => 'admin/module/flight',
                        'title'      => __('All Flights'),
                        'permission' => 'flight_view',
                    ],
                    'create'=>[
                        'url'        => 'admin/module/flight/create',
                        'title'      => __('Add new Flight'),
                        'permission' => 'flight_create',
                    ],
                    'attribute'=>[
                        'url'        => 'admin/module/flight/attribute',
                        'title'      => __('Attributes'),
                        'permission' => 'flight_manage_attributes',
                    ],
                    'availability'=>[
                        'url'        => 'admin/module/flight/availability',
                        'title'      => __('Availability'),
                        'permission' => 'flight_create',
                    ],

                ]
            ]
        ];
    }

    public static function getBookableServices()
    {
        if(!Flight::isEnable()) return [];
        return [
            'flight'=>Flight::class
        ];
    }

    public static function getMenuBuilderTypes()
    {
        if(!Flight::isEnable()) return [];
        return [
            'flight'=>[
                'class' => Flight::class,
                'name'  => __("Flight"),
                'items' => Flight::searchForMenu(),
                'position'=>51
            ]
        ];
    }

    public static function getUserMenu()
    {
        if(!Flight::isEnable()) return [];
        return [
            'flight' => [
                'url'   => route('flight.vendor.index'),
                'title'      => __("Manage Flight"),
                'icon'       => Flight::getServiceIconFeatured(),
                'position'   => 31,
                'permission' => 'flight_view',
                'children' => [
                    [
                        'url'   => route('flight.vendor.index'),
                        'title'  => __("All Flights"),
                    ],
                    [
                        'url'   => route('flight.vendor.create'),
                        'title'      => __("Add Flight"),
                        'permission' => 'flight_create',
                    ],
                    [
                        'url'        => route('flight.vendor.availability.index'),
                        'title'      => __("Availability"),
                        'permission' => 'flight_create',
                    ],
                    [
                        'url'   => route('flight.vendor.booking_report'),
                        'title'      => __("Booking Report"),
                        'permission' => 'flight_view',
                    ],
                ]
            ],
        ];
    }

    public static function getTemplateBlocks(){
        if(!Flight::isEnable()) return [];
        return [
            'form_search_flight'=>"\\Modules\\Flight\\Blocks\\FormSearchFlight",
            'list_flight'=>"\\Modules\\Flight\\Blocks\\ListFlight",
            'flight_term_featured_box'=>"\\Modules\\Flight\\Blocks\\FlightTermFeaturedBox",
        ];
    }
}
