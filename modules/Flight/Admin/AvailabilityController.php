<?php
namespace Modules\Flight\Admin;

use Modules\Flight\Models\FlightDate;

class AvailabilityController extends \Modules\Flight\Controllers\AvailabilityController
{
    protected $flightClass;
    /**
     * @var FlightDate
     */
    protected $flightDateClass;
    protected $indexView = 'Flight::admin.availability';

    public function __construct()
    {
        parent::__construct();
        $this->setActiveMenu('admin/module/flight');
        $this->middleware('dashboard');
    }
}
