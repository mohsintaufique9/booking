<style>
.value {
    font-weight : bold;
}

.booking-table tr {
    background: white!important;
    border-bottom: 1px solid rgb(233, 237, 241);
}

hr{
    display: block!important;
}
</style>

<div class="modal fade" id="modal-booking-print-{{$booking->id}}">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header pull-right float-right">
                <a class="btn m-1 btn-primary btn-info-booking pull-right float-right btn-print">
                    <i class="fa fa-print"></i>Print
                </a>
            </div>
       
            <!-- Modal body -->
            <div id="print-booking" class="modal-body">
                <h4 class="modal-title pb-2">{{__("Booking ID")}}: #{{$booking->id}}</h4>
                <hr>
                <div  class="row">
                   
                    <div class="col-md-6">
                        <h5 class="">Booking Information</h5>
                        <div class="table-responsive">
                            <table class="table table-borderless booking-table">
                                <thead>
                                    <th></th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="label">{{__('Booking Status')}}</td>
                                        <td class="value">{{$booking->statusName}}</td>
                                    </tr>
                                    <tr>
                                        <td class="label">{{__('Booking Date')}}</td>
                                        <td class="value">{{ display_date($booking->created_at) }}</td>
                                    </tr>

                                    @if(!empty($booking->gateway))
                                        <?php $gateway = get_payment_gateway_obj($booking->gateway);?>
                                        <tr>
                                            <td class="label">{{__('Payment Method')}}</td>
                                            <td class="val">{{$gateway->name}}</td>
                                        </tr>
                                    @endif
                        
                                    @include ($service->checkout_booking_detail_file_copy ?? '')
                                </tbody>
                            </table>
                            
                        </div>
                    </div>

                    <div class="col-md-6">
                        <h5 class="">User Information</h5>
                        <div class="table-responsive">
                            <table class="table table-borderless booking-table">
                                <thead>
                                    <th></th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    @include ($service->booking_customer_info_file ?? 'Booking::frontend/booking/booking-customer-info-print')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>   
            <!-- Modal footer -->
            <div class="modal-footer">
                <span class="btn btn-secondary" data-dismiss="modal">{{__("Close")}}</span>
            </div>
        </div>
    </div>
</div>
    