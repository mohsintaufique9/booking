@php
    $translation = $row->translateOrOrigin(app()->getLocale());
@endphp

<style>
.info {
    padding : 0!important;
}
</style>

<div class="item-loop {{$wrap_class ?? ''}}">
    <div class="item-title">
        <a @if(!empty($blank)) target="_blank" @endif href="{{$row->getDetailUrl($include_param ?? true)}}">
            @if($row->is_instant)
                <i class="fa fa-bolt d-none"></i>
            @endif
            {{$translation->title}}
        </a>
        @if($row->discount_percent)
            <div class="sale_info">{{$row->discount_percent}}</div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <div class="col-3 pl-3 pr-2">
                    <div class="location pt-2 pr-1">
                        @if(!empty($row->departure_time))
                            <h6>{{ \Carbon\Carbon::parse($row->departure_time)->format('H:i') }}</h6>
                        @endif
                    </div>
                    <div class="location pt-2 pr-1">
                        @if(!empty($row->arrival_time))
                            <h6>{{ \Carbon\Carbon::parse($row->arrival_time)->format('H:i') }}</h6>
                        @endif
                    </div>
                </div>
                <div class="col-2 pt-3">
                    <i class="fa fa-2x fa-road"></i>
                </div>
          
                <div class="col-4 p-1">
                    <div class="location p-1" >
                        @if(!empty($row->dLocation->name))
                            <h6 class="text-primary">{{ $row->dLocation->name }}</h6>
                        @endif
                    </div>
                    <div class="location p-1">
                        @if(!empty($row->aLocation->name))
                            <h6 class="text-primary">{{ $row->aLocation->name }}</h6>
                        @endif
                    </div>
                </div>
                <div class="col-3 pt-2 pl-0 pr-2">
                    <h6 class="text-dark text-sm-center"> {{ $row->getTravelTime() }}</h6>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            @if($row->getFCSeatsLeft() > 0)
                <div class="row p-2">
                    <div class="col-lg-4 col-3">
                        <span class="amenity total" data-toggle="tooltip"  title="{{ __("First Class") }}">
                            <span class="text-left">First Cls.</span>
                            <span class="text-right">({{$row->getFCSeatsLeft()}})</span>
                        </span>
                    </div>

                    <div class="col-lg-5 col-5">

                        <span class="info">
                            <div class="g-price">
                                <div class="prefix">
                                    <span class="fr_text">{{__("from")}}</span>
                                </div>
                                <div class="price">
                                    <span class="onsale">{{ format_money($row->fc_price) }}</span>
                                    <span class="text-price">{{ format_money($row->fc_sale_price) }}</span>
                                </div>
                            </div>
                        </span>
                    </div>

                    <div class="col-lg-3 col-4">
                        <form action="{{ route('train.add-to-cart', $row->id)}}" method="POST">
                            @csrf
                            <input type="hidden"name="type" value="fc">
                            <input type="hidden"name="start_date" value="{{ request()->input('start_date') }}">
                            <input type="submit" class="btn btn-sm btn-primary" value="Book Now"/>
                        </form>
                    </div>
                </div>
            @endif

            @if($row->getSCSeatsLeft() > 0)
                <div class="row p-2">
                    <div class="col-lg-4 col-3">
                        <span class="amenity total" data-toggle="tooltip"  title="{{ __("Second Class") }}">
                            <span class="text-left">Seond Cls.</span>
                            <span class="text-right">({{$row->getSCSeatsLeft() }})</span>
                        </span>
                    </div>

                    <div class="col-lg-5 col-5">

                        <span class="info">
                            <div class="g-price">
                                <div class="prefix">
                                    <span class="fr_text">{{__("from")}}</span>
                                </div>
                                <div class="price">
                                    <span class="onsale">{{ format_money($row->sc_price) }}</span>
                                    <span class="text-price">{{ format_money($row->sc_sale_price) }}</span>
                                </div>
                            </div>
                        </span>
                    </div>

                    <div class="col-lg-3 col-4 pull-right">
                        <form action="{{ route('train.add-to-cart', $row->id)}}" method="POST">
                            @csrf
                            <input type="hidden" name="type" value="sc">
                            <input type="hidden"name="start_date" value="{{ request()->input('start_date') }}">
                            <input type="submit" class="btn btn-sm btn-primary" value="Book Now"/>
                        </form>
                    </div>
                </div>
            @endif
            
            @if($row->getBCSeatsLeft() > 0)
                <div class="row  p-2">
                    <div class="col-lg-4 col-3">
                        <span class="amenity total" data-toggle="tooltip"  title="{{ __("Business Class") }}">
                            <span class="text-left">Business Cls.</span>
                            <span class="text-right">({{$row->getBCSeatsLeft() }})</span>
                        </span>
                    </div>

                    <div class="col-lg-5 col-5">

                        <span class="info">
                            <div class="g-price">
                                <div class="prefix">
                                    <span class="fr_text">{{__("from")}}</span>
                                </div>
                                <div class="price">
                                    <span class="onsale">{{ format_money($row->bc_price) }}</span>
                                    <span class="text-price">{{ format_money($row->bc_sale_price) }}</span>
                                </div>
                            </div>
                        </span>
                    </div>

                    <div class="col-lg-3 col-4">
                        <form action="{{ route('train.add-to-cart', $row->id)}}" method="POST">
                            @csrf
                            <input type="hidden"name="type" value="bc">
                            <input type="hidden"name="start_date" value="{{ request()->input('start_date') }}">
                            <input type="submit" class="btn btn-sm btn-primary" value="Book Now"/>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
