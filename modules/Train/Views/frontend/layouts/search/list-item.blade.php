<div class="row">
    <div class="col-lg-3 col-md-12">
        @include('Train::frontend.layouts.search.filter-search')
    </div>
    <div class="col-lg-9 col-md-12">
        <div class="bravo-list-item">
            <div class="topbar-search">
                <div class="text">
                    @if($rows->count() > 1)
                        {{ __(":count trains found",['count'=>$rows->count()]) }}
                    @else
                        {{ __(":count train found",['count'=>$rows->count()]) }}
                    @endif
                </div>
                <div class="control">
                </div>
            </div>
            <div class="list-item">
                <div class="row">
                    @if($rows->count() > 0)
                        @foreach($rows as $row)
                            <div class="col-lg-12 col-md-12">
                                @include('Train::frontend.layouts.search.loop-gird')
                            </div>
                        @endforeach
                    @else
                        <div class="col-lg-12">
                            {{__("Train not found")}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="bravo-pagination">
                {{$rows->appends(request()->query())->links()}}
                @if($rows->total() > 0)
                    <span class="count-string">{{ __("Showing :from - :to of :total Trains",["from"=>$rows->firstItem(),"to"=>$rows->lastItem(),"total"=>$rows->total()]) }}</span>
                @endif
            </div>
        </div>
    </div>
</div>
