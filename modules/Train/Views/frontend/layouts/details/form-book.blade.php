<div class="bravo_single_book_wrap">
    <div class="bravo_single_book">
        <div id="bravo_space_book_app" v-cloak>
            @if($row->discount_percent)
                <div class="tour-sale-box">
                    <span class="sale_class box_sale sale_small">{{$row->discount_percent}}</span>
                </div>
            @endif
            <div class="form-head">
               <div class="row">
                    <div class="col-md-4 pl-5">
                        <div class="location p-2 ">
                            @if(!empty($row->departure_time))
                                <h6>{{ \Carbon\Carbon::parse($row->departure_time)->format('H:i') }}</h6>
                            @endif
                        </div>
                        <div class="location p-2">
                            @if(!empty($row->arrival_time))
                                <h6>{{ \Carbon\Carbon::parse($row->arrival_time)->format('H:i') }}</h6>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2 pt-3 text-right">
                        <i class="fa fa-2x fa-road"></i>
                    </div>
                    <div class="col-md-6 float-right">
                        <div class="location p-2" >
                            @if(!empty($row->dLocation->name))
                                <h6 class="text-primary">{{ $row->dLocation->name }}</h6>
                            @endif
                        </div>
                        <div class="location p-2">
                            @if(!empty($row->aLocation->name))
                                <h6 class="text-primary">{{ $row->aLocation->name }}</h6>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-content">
                @if(Request::input('start'))
                    <div class="pl-3 pr-3 pt-3 d-flex">
                        <div class="flex-grow-1">
                            <label class="font-weight-bold">{{__("Selected Date:")}}</label>
                            <span class="text-primary"> {{ Request::input('start') }}</span>
                        </div>
                    </div>
                @else
                    <div class="form-group form-date-field form-date-search clearfix " data-format="{{get_moment_date_format()}}">
                        <div class="date-wrapper clearfix" @click="openStartDate">
                            <div class="check-in-wrapper">
                                <label>{{__("Select Date")}}</label>
                                <div class="render check-in-render" v-html="start_date_html"></div>
                            </div>
                            <i class="fa fa-angle-down arrow"></i>
                        </div>
                    </div>
                @endif
                <input type="text" class="start_date" ref="start_date" style="height: 1px; visibility: hidden" value="{{ Request::input('start') }}">

                @if($row->getFCSeatsLeft() > 0 )
                    <div class="form-group form-guest-search">
                        <div class="guest-wrapper d-flex justify-content-between align-items-center">
                            <div class="flex-grow-1">
                                <label>{{__("First Cls.")}}</label>
                                <span> ({{ $row->getFCSeatsLeft() }})</span>
                                <p class="text-primary">{{ format_money($row->getFCPrice()) }}</p>
                            </div>
                            <div class="flex-shrink-0">
                                <div class="input-number-group">
                                    <i class="icon ion-ios-remove-circle-outline" @click="minusNumberTypeFC()"></i>
                                    <span class="input">@{{fc_number}}</span>
                                    <i class="icon ion-ios-add-circle-outline" @click="addNumberTypeFC()"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                
                @if($row->getSCSeatsLeft() > 0 )
                    <div class="form-group form-guest-search">
                        <div class="guest-wrapper d-flex justify-content-between align-items-center">
                            <div class="flex-grow-1">
                                <label>{{__("Second Cls.")}}</label>
                                <span> ({{ $row->getSCSeatsLeft() }})</span>
                                <p class="text-primary">{{ format_money($row->getSCPrice()) }}</p>
                            </div>
                            <div class="flex-shrink-0">
                                <div class="input-number-group">
                                    <i class="icon ion-ios-remove-circle-outline" @click="minusNumberTypeSC()"></i>
                                    <span class="input">@{{ sc_number}}</span>
                                    <i class="icon ion-ios-add-circle-outline" @click="addNumberTypeSC()"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if($row->getBCSeatsLeft() > 0 )
                    <div class="form-group form-guest-search">
                        <div class="guest-wrapper d-flex justify-content-between align-items-center">
                            <div class="flex-grow-1">
                                <label>{{__("Business Cls.")}}</label>
                                <span> ({{ $row->getBCSeatsLeft() }})</span>
                                <p class="text-primary">{{ format_money($row->getBCPrice()) }}</p>
                            </div>
                            <div class="flex-shrink-0">
                                <div class="input-number-group">
                                    <i class="icon ion-ios-remove-circle-outline" @click="minusNumberTypeBC()"></i>
                                    <span class="input">@{{  bc_number}}</span>
                                    <i class="icon ion-ios-add-circle-outline" @click="addNumberTypeBC()"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="form-section-group form-group" v-if="extra_price.length">
                    <h4 class="form-section-title">{{__('Extra prices:')}}</h4>
                    <div class="form-group " v-for="(type,index) in extra_price">
                        <div class="extra-price-wrap d-flex justify-content-between">
                            <div class="flex-grow-1">
                                <label><input type="checkbox" v-model="type.enable"> @{{type.name}}</label>
                                <div class="render" v-if="type.price_type">(@{{type.price_type}})</div>
                            </div>
                            <div class="flex-shrink-0">@{{type.price_html}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-section-group form-group-padding" v-if="buyer_fees.length">
                    <div class="extra-price-wrap d-flex justify-content-between" v-for="(type,index) in buyer_fees">
                        <div class="flex-grow-1">
                            <label>@{{type.type_name}}
                                <i class="icofont-info-circle" v-if="type.desc" data-toggle="tooltip" data-placement="top" :title="type.type_desc"></i>
                            </label>
                            <div class="render" v-if="type.price_type">(@{{type.price_type}})</div>
                        </div>
                        <div class="flex-shrink-0">@{{formatMoney(type.price)}}
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-section-total" v-if="total_price > 0">
                <label>{{__("Total")}}</label>
                <span class="price">@{{ total_price_html }}</span>
            </div>
            <div v-html="html"></div>
            <div class="submit-group">
                <a class="btn btn-large" @click="doSubmit($event)" :class="{'disabled':onSubmit,'btn-success':(step == 2),'btn-primary':step == 1}" name="submit">
                    <span v-if="step == 1">{{__("BOOK NOW")}}</span>
                    <span v-if="step == 2">{{__("Book Now")}}</span>
                    <i v-show="onSubmit" class="fa fa-spinner fa-spin"></i>
                </a>
                <div class="alert-text mt10" v-show="message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></div>
            </div>
        </div>
    </div>
</div>
