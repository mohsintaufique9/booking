<?php
namespace Modules\Train\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Modules\Core\Models\Terms;

class TrainTermFeaturedBox extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Title')
                ],
                [
                    'id'        => 'desc',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Desc')
                ],
                [
                    'id'           => 'term_train',
                    'type'         => 'select2',
                    'label'        => __('Select term train'),
                    'select2'      => [
                        'ajax'     => [
                            'url'      => route('train.admin.attribute.term.getForSelect2', ['type' => 'train']),
                            'dataType' => 'json'
                        ],
                        'width'    => '100%',
                        'multiple' => "true",
                    ],
                    'pre_selected' => route('train.admin.attribute.term.getForSelect2', [
                        'type'         => 'space',
                        'pre_selected' => 1
                    ])
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('Train: Term Featured Box');
    }

    public function content($model = [])
    {
        if (empty($term_space = $model['term_train'])) {
            return "";
        }
        $list_term = Terms::whereIn('id',$term_space)->get();
        $model['list_term'] = $list_term;
        return view('Train::frontend.blocks.term-featured-box.index', $model);
    }
}