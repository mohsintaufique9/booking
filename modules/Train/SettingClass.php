<?php

namespace  Modules\Train;

use Modules\Core\Abstracts\BaseSettingsClass;
use Modules\Core\Models\Settings;

class SettingClass extends BaseSettingsClass
{
    public static function getSettingPages()
    {
        return [
            [
                'id'   => 'train',
                'title' => __("Train Settings"),
                'position'=>20,
                'view'=>"Train::admin.settings.train",
                "keys"=>[
                    'train_disable',
                    'train_page_search_title',
                    'train_page_search_banner',
                    'train_layout_search',
                    'train_location_search_style',

                    'train_enable_review',
                    'train_review_approved',
                    'train_enable_review_after_booking',
                    'train_review_number_per_page',
                    'train_review_stats',

                    'train_page_list_seo_title',
                    'train_page_list_seo_desc',
                    'train_page_list_seo_image',
                    'train_page_list_seo_share',

                    'train_booking_buyer_fees',
                    'train_vendor_create_service_must_approved_by_admin',
                    'train_allow_vendor_can_change_their_booking_status',
                    'train_map_search_fields'
                ],
                'html_keys'=>[

                ]
            ]
        ];
    }
}
