<?php
use \Illuminate\Support\Facades\Route;

Route::group(['prefix'=>config('train.train_route_prefix')],function(){
    Route::get('/','TrainController@index')->name('train.search'); // Search
    Route::get('/{slug}','TrainController@detail')->name('train.detail');// Detail
    Route::get('/booking/{slug}','TrainController@booking')->name('train.booking');// Booking
    Route::post('/booking/add-to-cart/{id}','TrainController@addToCart')->name('train.add-to-cart');// cart
});

Route::group(['prefix'=>'user/'.config('train.train_route_prefix')],function(){

    Route::match(['get','post'],'/','ManageTrainController@manageTrain')->name('train.vendor.index');
    Route::match(['get','post'],'/create','ManageTrainController@createTrain')->name('train.vendor.create');
    Route::match(['get','post'],'/edit/{slug}','ManageTrainController@editTrain')->name('train.vendor.edit');
    Route::match(['get','post'],'/del/{slug}','ManageTrainController@deleteTrain')->name('train.vendor.delete');
    Route::match(['post'],'/store/{slug}','ManageTrainController@store')->name('train.vendor.store');
    Route::get('bulkEdit/{id}','ManageTrainController@bulkEditTrain')->name("train.vendor.bulk_edit");
    Route::get('/booking-report','ManageTrainController@bookingReport')->name("train.vendor.booking_report");
    Route::get('/booking-report/bulkEdit/{id}','ManageTrainController@bookingReportBulkEdit')->name("train.vendor.booking_report.bulk_edit");

    Route::group(['prefix'=>'availability'],function(){
        Route::get('/','AvailabilityController@index')->name('train.vendor.availability.index');
        Route::get('/loadDates','AvailabilityController@loadDates')->name('train.vendor.availability.loadDates');
        Route::match(['get','post'],'/store','AvailabilityController@store')->name('train.vendor.availability.store');
    });
});