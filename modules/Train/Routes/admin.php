<?php

use \Illuminate\Support\Facades\Route;


Route::get('/','TrainController@index')->name('train.admin.index');
Route::get('/create','TrainController@create')->name('train.admin.create');
Route::get('/edit/{id}','TrainController@edit')->name('train.admin.edit');
Route::post('/store/{id}','TrainController@store')->name('train.admin.store');
Route::post('/bulkEdit','TrainController@bulkEdit')->name('train.admin.bulkEdit');
Route::post('/bulkEdit','TrainController@bulkEdit')->name('train.admin.bulkEdit');

Route::group(['prefix'=>'attribute'],function (){
    Route::get('/','AttributeController@index')->name('train.admin.attribute.index');
    Route::get('edit/{id}','AttributeController@edit')->name('train.admin.attribute.edit');
    Route::post('store/{id}','AttributeController@store')->name('train.admin.attribute.store');

    Route::get('terms/{id}','AttributeController@terms')->name('train.admin.attribute.term.index');
    Route::get('term_edit/{id}','AttributeController@term_edit')->name('train.admin.attribute.term.edit');
    Route::get('term_store','AttributeController@term_store')->name('train.admin.attribute.term.store');

    Route::get('getForSelect2','AttributeController@getForSelect2')->name('train.admin.attribute.term.getForSelect2');
});

Route::group(['prefix'=>'availability'],function(){
    Route::get('/','AvailabilityController@index')->name('train.admin.availability.index');
    Route::get('/loadDates','AvailabilityController@loadDates')->name('train.admin.availability.loadDates');
    Route::match(['get','post'],'/store','AvailabilityController@store')->name('train.admin.availability.store');
});
