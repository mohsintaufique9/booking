<?php
namespace Modules\Train\Admin;

use Modules\Train\Models\TrainDate;

class AvailabilityController extends \Modules\Train\Controllers\AvailabilityController
{
    protected $trainClass;
    /**
     * @var TrainDate
     */
    protected $trainDateClass;
    protected $indexView = 'Train::admin.availability';

    public function __construct()
    {
        parent::__construct();
        $this->setActiveMenu('admin/module/train');
        $this->middleware('dashboard');
    }
}
