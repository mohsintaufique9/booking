<?php
namespace Modules\Train;
use Modules\Train\Models\Train;
use Modules\ModuleServiceProvider;

class ModuleProvider extends ModuleServiceProvider
{

    public function boot(){

        $this->loadMigrationsFrom(__DIR__ . '/Migrations');

    }
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouterServiceProvider::class);
    }

    public static function getAdminMenu()
    {
        if(!Train::isEnable()) return [];
        return [
            'train'=>[
                "position"=>45,
                'url'        => 'admin/module/train',
                'title'      => __('Train'),
                'icon'       => 'ion-logo-model-s',
                'permission' => 'train_view',
                'children'   => [
                    'add'=>[
                        'url'        => 'admin/module/train',
                        'title'      => __('All Trains'),
                        'permission' => 'train_view',
                    ],
                    'create'=>[
                        'url'        => 'admin/module/train/create',
                        'title'      => __('Add new Train'),
                        'permission' => 'train_create',
                    ],
                    'attribute'=>[
                        'url'        => 'admin/module/train/attribute',
                        'title'      => __('Attributes'),
                        'permission' => 'train_manage_attributes',
                    ],
                    'availability'=>[
                        'url'        => 'admin/module/train/availability',
                        'title'      => __('Availability'),
                        'permission' => 'train_create',
                    ],

                ]
            ]
        ];
    }

    public static function getBookableServices()
    {
        if(!Train::isEnable()) return [];
        return [
            'train'=>Train::class
        ];
    }

    public static function getMenuBuilderTypes()
    {
        if(!Train::isEnable()) return [];
        return [
            'train'=>[
                'class' => Train::class,
                'name'  => __("Train"),
                'items' => Train::searchForMenu(),
                'position'=>51
            ]
        ];
    }

    public static function getUserMenu()
    {
        if(!Train::isEnable()) return [];
        return [
            'train' => [
                'url'   => route('train.vendor.index'),
                'title'      => __("Manage Train"),
                'icon'       => Train::getServiceIconFeatured(),
                'position'   => 31,
                'permission' => 'train_view',
                'children' => [
                    [
                        'url'   => route('train.vendor.index'),
                        'title'  => __("All Trains"),
                    ],
                    [
                        'url'   => route('train.vendor.create'),
                        'title'      => __("Add Train"),
                        'permission' => 'train_create',
                    ],
                    [
                        'url'        => route('train.vendor.availability.index'),
                        'title'      => __("Availability"),
                        'permission' => 'train_create',
                    ],
                    [
                        'url'   => route('train.vendor.booking_report'),
                        'title'      => __("Booking Report"),
                        'permission' => 'train_view',
                    ],
                ]
            ],
        ];
    }

    public static function getTemplateBlocks(){
        if(!Train::isEnable()) return [];
        return [
            'form_search_train'=>"\\Modules\\Train\\Blocks\\FormSearchTrain",
            'list_train'=>"\\Modules\\Train\\Blocks\\ListTrain",
            'train_term_featured_box'=>"\\Modules\\Train\\Blocks\\TrainTermFeaturedBox",
        ];
    }
}
