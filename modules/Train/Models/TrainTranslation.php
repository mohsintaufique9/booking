<?php

namespace Modules\Train\Models;

use App\BaseModel;

class TrainTranslation extends Train
{
    protected $table = 'bravo_train_translations';

    protected $fillable = [
        'title',
        'content',
        'faqs',
        'address',
        'extra_price'
    ];

    protected $slugField     = false;
    protected $seo_type = 'train_translation';

    protected $cleanFields = [
        'content'
    ];
    protected $casts = [
        'faqs'  => 'array',
        'extra_price'  => 'array',
    ];

    public function getSeoType(){
        return $this->seo_type;
    }
}