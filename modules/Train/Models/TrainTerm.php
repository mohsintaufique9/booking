<?php
namespace Modules\Train\Models;

use App\BaseModel;

class TrainTerm extends BaseModel
{
    protected $table = 'bravo_train_term';
    protected $fillable = [
        'term_id',
        'target_id'
    ];
}