<?php
namespace Modules\Bus;
use Modules\Bus\Models\Bus;
use Modules\ModuleServiceProvider;

class ModuleProvider extends ModuleServiceProvider
{

    public function boot(){

        $this->loadMigrationsFrom(__DIR__ . '/Migrations');

    }
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouterServiceProvider::class);
    }

    public static function getAdminMenu()
    {
        if(!Bus::isEnable()) return [];
        return [
            'bus'=>[
                "position"=>45,
                'url'        => 'admin/module/bus',
                'title'      => __('Bus'),
                'icon'       => 'ion-logo-model-s',
                'permission' => 'bus_view',
                'children'   => [
                    'add'=>[
                        'url'        => 'admin/module/bus',
                        'title'      => __('All Buses'),
                        'permission' => 'bus_view',
                    ],
                    'create'=>[
                        'url'        => 'admin/module/bus/create',
                        'title'      => __('Add new Bus'),
                        'permission' => 'bus_create',
                    ],
                    'attribute'=>[
                        'url'        => 'admin/module/bus/attribute',
                        'title'      => __('Attributes'),
                        'permission' => 'bus_manage_attributes',
                    ],
                    'availability'=>[
                        'url'        => 'admin/module/bus/availability',
                        'title'      => __('Availability'),
                        'permission' => 'bus_create',
                    ],

                ]
            ]
        ];
    }

    public static function getBookableServices()
    {
        if(!Bus::isEnable()) return [];
        return [
            'bus'=>Bus::class
        ];
    }

    public static function getMenuBuilderTypes()
    {
        if(!Bus::isEnable()) return [];
        return [
            'bus'=>[
                'class' => Bus::class,
                'name'  => __("Bus"),
                'items' => Bus::searchForMenu(),
                'position'=>51
            ]
        ];
    }

    public static function getUserMenu()
    {
        if(!Bus::isEnable()) return [];
        return [
            'bus' => [
                'url'   => route('bus.vendor.index'),
                'title'      => __("Manage Bus"),
                'icon'       => Bus::getServiceIconFeatured(),
                'position'   => 31,
                'permission' => 'bus_view',
                'children' => [
                    [
                        'url'   => route('bus.vendor.index'),
                        'title'  => __("All Buses"),
                    ],
                    [
                        'url'   => route('bus.vendor.create'),
                        'title'      => __("Add Bus"),
                        'permission' => 'bus_create',
                    ],
                    [
                        'url'        => route('bus.vendor.availability.index'),
                        'title'      => __("Availability"),
                        'permission' => 'bus_create',
                    ],
                    [
                        'url'   => route('bus.vendor.booking_report'),
                        'title'      => __("Booking Report"),
                        'permission' => 'bus_view',
                    ],
                ]
            ],
        ];
    }

    public static function getTemplateBlocks(){
        if(!Bus::isEnable()) return [];
        return [
            'form_search_bus'=>"\\Modules\\Bus\\Blocks\\FormSearchBus",
            'list_bus'=>"\\Modules\\Bus\\Blocks\\ListBus",
            'bus_term_featured_box'=>"\\Modules\\Bus\\Blocks\\BusTermFeaturedBox",
        ];
    }
}
