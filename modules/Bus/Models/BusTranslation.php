<?php

namespace Modules\Bus\Models;

use App\BaseModel;

class BusTranslation extends Bus
{
    protected $table = 'bravo_bus_translations';

    protected $fillable = [
        'title',
        'content',
        'faqs',
        'address',
        'extra_price'
    ];

    protected $slugField     = false;
    protected $seo_type = 'bus_translation';

    protected $cleanFields = [
        'content'
    ];
    protected $casts = [
        'faqs'  => 'array',
        'extra_price'  => 'array',
    ];

    public function getSeoType(){
        return $this->seo_type;
    }
}