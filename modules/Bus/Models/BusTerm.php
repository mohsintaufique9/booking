<?php
namespace Modules\Bus\Models;

use App\BaseModel;

class BusTerm extends BaseModel
{
    protected $table = 'bravo_bus_term';
    protected $fillable = [
        'term_id',
        'target_id'
    ];
}