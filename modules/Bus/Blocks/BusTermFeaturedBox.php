<?php
namespace Modules\Bus\Blocks;

use Modules\Template\Blocks\BaseBlock;
use Modules\Core\Models\Terms;

class BusTermFeaturedBox extends BaseBlock
{
    function __construct()
    {
        $this->setOptions([
            'settings' => [
                [
                    'id'        => 'title',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Title')
                ],
                [
                    'id'        => 'desc',
                    'type'      => 'input',
                    'inputType' => 'text',
                    'label'     => __('Desc')
                ],
                [
                    'id'           => 'term_bus',
                    'type'         => 'select2',
                    'label'        => __('Select term bus'),
                    'select2'      => [
                        'ajax'     => [
                            'url'      => route('bus.admin.attribute.term.getForSelect2', ['type' => 'bus']),
                            'dataType' => 'json'
                        ],
                        'width'    => '100%',
                        'multiple' => "true",
                    ],
                    'pre_selected' => route('bus.admin.attribute.term.getForSelect2', [
                        'type'         => 'space',
                        'pre_selected' => 1
                    ])
                ],
            ]
        ]);
    }

    public function getName()
    {
        return __('Bus: Term Featured Box');
    }

    public function content($model = [])
    {
        if (empty($term_space = $model['term_bus'])) {
            return "";
        }
        $list_term = Terms::whereIn('id',$term_space)->get();
        $model['list_term'] = $list_term;
        return view('Bus::frontend.blocks.term-featured-box.index', $model);
    }
}