<?php
namespace Modules\Bus\Admin;

use Modules\Bus\Models\BusDate;

class AvailabilityController extends \Modules\Bus\Controllers\AvailabilityController
{
    protected $busClass;
    /**
     * @var BusDate
     */
    protected $busDateClass;
    protected $indexView = 'Bus::admin.availability';

    public function __construct()
    {
        parent::__construct();
        $this->setActiveMenu('admin/module/bus');
        $this->middleware('dashboard');
    }
}
