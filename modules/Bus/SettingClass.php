<?php

namespace  Modules\Bus;

use Modules\Core\Abstracts\BaseSettingsClass;
use Modules\Core\Models\Settings;

class SettingClass extends BaseSettingsClass
{
    public static function getSettingPages()
    {
        return [
            [
                'id'   => 'bus',
                'title' => __("Bus Settings"),
                'position'=>20,
                'view'=>"Bus::admin.settings.bus",
                "keys"=>[
                    'bus_disable',
                    'bus_page_search_title',
                    'bus_page_search_banner',
                    'bus_layout_search',
                    'bus_location_search_style',

                    'bus_enable_review',
                    'bus_review_approved',
                    'bus_enable_review_after_booking',
                    'bus_review_number_per_page',
                    'bus_review_stats',

                    'bus_page_list_seo_title',
                    'bus_page_list_seo_desc',
                    'bus_page_list_seo_image',
                    'bus_page_list_seo_share',

                    'bus_booking_buyer_fees',
                    'bus_vendor_create_service_must_approved_by_admin',
                    'bus_allow_vendor_can_change_their_booking_status',
                    'bus_map_search_fields'
                ],
                'html_keys'=>[

                ]
            ]
        ];
    }
}
