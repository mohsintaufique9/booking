<?php

use \Illuminate\Support\Facades\Route;


Route::get('/','BusController@index')->name('bus.admin.index');
Route::get('/create','BusController@create')->name('bus.admin.create');
Route::get('/edit/{id}','BusController@edit')->name('bus.admin.edit');
Route::post('/store/{id}','BusController@store')->name('bus.admin.store');
Route::post('/bulkEdit','BusController@bulkEdit')->name('bus.admin.bulkEdit');
Route::post('/bulkEdit','BusController@bulkEdit')->name('bus.admin.bulkEdit');

Route::group(['prefix'=>'attribute'],function (){
    Route::get('/','AttributeController@index')->name('bus.admin.attribute.index');
    Route::get('edit/{id}','AttributeController@edit')->name('bus.admin.attribute.edit');
    Route::post('store/{id}','AttributeController@store')->name('bus.admin.attribute.store');

    Route::get('terms/{id}','AttributeController@terms')->name('bus.admin.attribute.term.index');
    Route::get('term_edit/{id}','AttributeController@term_edit')->name('bus.admin.attribute.term.edit');
    Route::get('term_store','AttributeController@term_store')->name('bus.admin.attribute.term.store');

    Route::get('getForSelect2','AttributeController@getForSelect2')->name('bus.admin.attribute.term.getForSelect2');
});

Route::group(['prefix'=>'availability'],function(){
    Route::get('/','AvailabilityController@index')->name('bus.admin.availability.index');
    Route::get('/loadDates','AvailabilityController@loadDates')->name('bus.admin.availability.loadDates');
    Route::match(['get','post'],'/store','AvailabilityController@store')->name('bus.admin.availability.store');
});
