<?php
use \Illuminate\Support\Facades\Route;

Route::group(['prefix'=>config('bus.bus_route_prefix')],function(){
    Route::get('/','BusController@index')->name('bus.search'); // Search
    Route::get('/{slug}','BusController@detail')->name('bus.detail');// Detail
    Route::get('/booking/{slug}','BusController@booking')->name('bus.booking');// Booking
    Route::post('/booking/add-to-cart/{id}','BusController@addToCart')->name('bus.add-to-cart');// cart
});

Route::group(['prefix'=>'user/'.config('bus.bus_route_prefix')],function(){

    Route::match(['get','post'],'/','ManageBusController@manageBus')->name('bus.vendor.index');
    Route::match(['get','post'],'/create','ManageBusController@createBus')->name('bus.vendor.create');
    Route::match(['get','post'],'/edit/{slug}','ManageBusController@editBus')->name('bus.vendor.edit');
    Route::match(['get','post'],'/del/{slug}','ManageBusController@deleteBus')->name('bus.vendor.delete');
    Route::match(['post'],'/store/{slug}','ManageBusController@store')->name('bus.vendor.store');
    Route::get('bulkEdit/{id}','ManageBusController@bulkEditBus')->name("bus.vendor.bulk_edit");
    Route::get('/booking-report','ManageBusController@bookingReport')->name("bus.vendor.booking_report");
    Route::get('/booking-report/bulkEdit/{id}','ManageBusController@bookingReportBulkEdit')->name("bus.vendor.booking_report.bulk_edit");

    Route::group(['prefix'=>'availability'],function(){
        Route::get('/','AvailabilityController@index')->name('bus.vendor.availability.index');
        Route::get('/loadDates','AvailabilityController@loadDates')->name('bus.vendor.availability.loadDates');
        Route::match(['get','post'],'/store','AvailabilityController@store')->name('bus.vendor.availability.store');
    });
});