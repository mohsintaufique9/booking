@php $lang_local = app()->getLocale() @endphp

@if($booking->start_date)
    <tr>
        <td class="label">{{__('Start date:')}}</td>
        <td class="val">
            {{display_date($booking->start_date)}}
        </td>
    </tr>
    <tr>
        <td class="label">{{__('End date:')}}</td>
        <td class="val">
            {{display_date($booking->end_date)}}
        </td>
    </tr>
    <tr>
        <td class="label">{{__('Nights:')}}</td>
        <td class="val">
            {{$booking->duration_nights}}
        </td>
    </tr>
@endif
@if($meta = $booking->getMeta('adults'))
    <tr>
        <td class="label">{{__('Adults:')}}</td>
        <td class="val">
            {{$meta}}
        </td>
    </tr>
@endif
@if($meta = $booking->getMeta('children'))
    <tr>
        <td class="label">{{__('Children:')}}</td>
        <td class="val">
            {{$meta}}
        </td>
    </tr>
@endif
<tr>
    <td class="label">{{format_money($booking->total_before_fees/$booking->duration_nights)}} *
        @if($booking->duration_nights <= 1)
            {{__(':count night',['count'=>$booking->duration_nights])}}
        @else
            {{__(':count nights',['count'=>$booking->duration_nights])}}
        @endif
    </td>
    <td class="val">
        {{ !empty($booking->total_before_fees) ? format_money($booking->total_before_fees) : format_money($booking->total) }}
    </td>
</tr>
@php $extra_price = $booking->getJsonMeta('extra_price') @endphp
@if(!empty($extra_price))
    <tr>
        <td class="label-title"><strong>{{__("Extra Prices:")}}</strong></td>
    </tr>
    <tr class="no-flex">
        <ul>
            @foreach($extra_price as $type)
                <tr>
                    <td class="label">{{$type['name_'.$lang_local] ?? $type['name']}}:</td>
                    <td class="val">
                        {{format_money($type['total'] ?? 0)}}
                    </td>
                </tr>
            @endforeach
        </ul>
    </tr>
@endif
@if(!empty($booking->buyer_fees))
    <?php
    $buyer_fees = json_decode($booking->buyer_fees , true);
    foreach ($buyer_fees as $buyer_fee){
        ?>
        <tr>
            <td class="label">
                {{$buyer_fee['name_'.$lang_local] ?? $buyer_fee['name']}}
                <i class="icofont-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $buyer_fee['desc_'.$lang_local] ?? $buyer_fee['desc'] }}"></i>
                @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                    : {{$booking->total_guests}} * {{format_money( $buyer_fee['price'] )}}
                @endif
            </td>
            <td class="val">
                @if(!empty($buyer_fee['per_person']) and $buyer_fee['per_person'] == "on")
                    {{ format_money( $buyer_fee['price'] * $booking->total_guests ) }}
                @else
                {{ format_money($buyer_fee['price']) }}
                @endif
            </td>
        </tr>
    <?php } ?>
@endif
<tr class="final-total">
    <td class="label">{{__("Total:")}}</td>
    <td class="val">{{format_money($booking->total)}}</td>
</tr>
       
   