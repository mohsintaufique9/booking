
<div class="container">
    <div class="sectiontitle">
        <h3 class="text-center">Our Numbers</h3>
        <span class="headerLine"></span>
    </div>
    <div class="row"
        <div id="projectFacts" class="sectionClass">
            <div class="fullWidth eight columns">
                <div class="projectFactsWrap ">
                    @php 
                        $bookings = \Modules\Booking\Models\Booking::count();
                        $users = \Modules\User\Models\User::count();
                        $services = count(get_bookable_services());
                    @endphp
                    <div class="item wow fadeInUpBig animated animated" style="visibility: visible;">
                        <i class="fa fa-briefcase"></i>
                        <p id="number1" data-number="{{ $bookings }}" class="number"></p>
                        <span></span>
                        <p>Trips Booked</p>
                    </div>
                    <div class="item wow fadeInUpBig animated animated" style="visibility: visible;">
                        <i class="fa fa-smile-o"></i>
                        <p id="number2" data-number="{{ $users }}" class="number"></p>
                        <span></span>
                        <p>Happy clients</p>
                    </div>
                    <div class="item wow fadeInUpBig animated animated" style="visibility: visible;">
                        <i class="fa fa-users"></i>
                        <p id="number3" data-number="{{ $bookings }}" class="number"></p>
                        <span></span>
                        <p>Our Partners</p>
                    </div>
                    <div class="item wow fadeInUpBig animated animated" style="visibility: visible;">
                        <i class="fa fa-gear"></i>
                        <p id="number4"data-number="{{ $services }}" class="number"></p>
                        <span></span>
                        <p>Our Services</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
