@extends('layouts.app')
@section('head')
    <link href="{{ asset('module/booking/css/checkout.css?_ver='.config('app.version')) }}" rel="stylesheet">
@endsection
@section('content')
    <div class="bravo-booking-page padding-content" >
        <div class="container">
            @if($booking->status == 'confirmed')
                <h5 class="text-primary confirm-text">
                <span class="timer text-danger"></span><span class="timer-text"></span></span>
                </h5>
                <form action="{{ route('booking.payment', $booking->code)}}" method="post">
                    @csrf
                    @include ($service->checkout_form_payment_file ?? 'Booking::frontend/booking/checkout-payment')
                </form>
            @else

                <div class="row booking-success-notice">
                    <div class="col-lg-8 col-md-8">
                        <div class="d-flex align-items-center">
                            <img src="{{url('images/ico_success.svg')}}" alt="Payment Success">
                            <div class="notice-success">
                                <p class="line1"><span>{{$booking->first_name}},</span>
                                    {{__('your order was submitted successfully!')}}
                                </p>
                                <p class="line2">{{__('Booking details has been sent to:')}} <span>{{$booking->email}}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <ul class="booking-info-detail">
                            <li><span>{{__('Booking Number')}}:</span> {{$booking->id}}</li>
                            <li><span>{{__('Booking Date')}}:</span> {{display_date($booking->created_at)}}</li>
                            @if(!empty($gateway))
                            <li><span>{{__('Payment Method')}}:</span> {{$gateway->name}}</li>
                            @endif
                        </ul>
                    </div>
                </div>
            @endif

            <div class="row booking-success-detail">
                <div class="col-md-8">
                    @include ($service->booking_customer_info_file ?? 'Booking::frontend/booking/booking-customer-info')
                    <div class="text-center">
                        <a href="{{url(app_get_locale().'/user/booking-history')}}" class="btn btn-primary">{{__('Booking History')}}</a>
                    </div>
                </div>
                <div class="col-md-4">
                    @include ($service->checkout_booking_detail_file ?? '')
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')

 <script>

showTimer();

function showTimer() {

    var time = "{{\Carbon\Carbon::now()->diffInSeconds(\Carbon\Carbon::parse($booking->updated_at)->addMinutes(10), false) }}";
    console.log(time);
    if(time <= 0) {
        $('.confirm-text').html('The 10 minutes time bracket has ended. Your booking might be cancelled now');
    } else {
        CountDown(time, $('.timer'));
        setTimeout(function() {
            $('.timer-text').html(' Please finish payment within the time limit or your booking might be canceled.')
        }, 600);
    }
}

function CountDown(duration, display) {

    if (!isNaN(duration)) {
        var timer = duration, minutes, seconds;

        var interVal = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            $(display).html("<b>" + minutes + "m : " + seconds + "s" + "</b>");
            if (--timer < 0) {
                $('.confirm-text').html('The 10 minutes time bracket has ended. Your booking might be cancelled now');
            }
        }, 1000);
    }
}
</script>
@endsection