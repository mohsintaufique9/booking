

<tr class="info-first-name">
    <td class="label">{{__('First name')}}</td>
    <td class="val">{{$booking->first_name}}</td>
</tr>
<tr class="info-last-name">
    <td class="label">{{__('Last name')}}</td>
    <td class="val">{{$booking->last_name}}</td>
</tr>
<tr class="info-email">
    <td class="label">{{__('Email')}}</td>
    <td class="val">{{$booking->email}}</td>
</tr>
<tr class="info-phone">
    <td class="label">{{__('Phone')}}</td>
    <td class="val">{{$booking->phone}}</td>
</tr>
<tr class="info-address">
    <td class="label">{{__('ID Type')}}</td>
    <td class="val">{{$booking->getIDType()}}</td>
</tr>
<tr class="info-address2">
    <td class="label">{{__('ID Number')}}</td>
    <td class="val">{{$booking->id_number}}</td>
</tr>
<tr class="info-country">
    <td class="label">{{__('Country')}}</td>
    <td class="val">{{get_country_name($booking->country)}}</td>
</tr>


