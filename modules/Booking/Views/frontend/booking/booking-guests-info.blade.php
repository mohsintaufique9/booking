@if($booking->guests->count() > 0)

<div class="booking-review">
    <h4 class="booking-review-title">{{__('Your Guests')}}</h4>
    @foreach($booking->guests as $guest)
    <div class="booking-review-content">
        <div class="review-section">
            @if($display == 0)
                <div class="row">
                    <div class="col-md-10"></div>
                    <div class="col-md-2 text-right mb-5">
                
                        <a href="{{ route('booking.remove-passenger', $guest->id) }}"class="btn btn-sm btn-danger">
                            <i class="fa fa-minus"></i>
                        </a>
                    </div>
                </div>
            @endif
            <div class="info-form">
                <ul>
                    <li class="info-first-name">
                        <div class="label">{{__('First name')}}</div>
                        <div class="val">{{$guest->first_name}}</div>
                    </li>
                    <li class="info-last-name">
                        <div class="label">{{__('Last name')}}</div>
                        <div class="val">{{$guest->last_name}}</div>
                    </li>
                    <li class="info-email">
                        <div class="label">{{__('ID Number')}}</div>
                        <div class="val">{{$guest->getIDType()}}</div>
                    </li>

                    <li class="info-email">
                        <div class="label">{{__('ID Type')}}</div>
                        <div class="val">{{$guest->id_number}}</div>
                    </li>

                    <li class="info-email">
                        <div class="label">{{__('Type')}}</div>
                        <div class="val">{{$guest->getType()}}</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif
