<div class="form-checkout" id="form-checkout" >
    <input type="hidden" name="code" value="{{$booking->code}}">
    <div class="form-section">
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label >{{__("ID")}} <span class="required">*</span> </label>
                    <select required name="id" class="form-control id_type">
                        <option value="">{{__('-- Select --')}}</option>
                        <option @if($user->id_type == 0) selected @endif value="0">Tanzanian ID</option>
                        <option @if($user->id_type == 1) selected @endif value="1">Passport</option>
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label >{{__("ID Number")}} <span class="required">*</span></label>
                    <input type="text" minlength="20" maxlength="20" required placeholder="{{__("Enter ID Number")}}" class="form-control id-number" value="{{ $user->id_number ?? ''}}" name="id_num">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>{{__("Expiration Date")}}</label>
                    <input type="date" required name="exp_date" placeholder="{{__("Expiration Date")}}" min="{{ \Carbon\Carbon::now()->toDateString()}}" value="{{ $user->expiration_date}}" class="form-control">
                </div>
            </div>

            <div class="col-md-6 field-email">
                <div class="form-group">
                    <label >{{__("Email")}} <span class="required">*</span></label>
                    <input type="email" placeholder="{{__("email@domain.com")}}" class="form-control" value="{{$user->email ?? ''}}" name="email">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label >{{__("First Name")}} <span class="required">*</span></label>
                    <input type="text" placeholder="{{__("First Name")}}" class="form-control" value="{{$user->first_name ?? ''}}" name="first_name">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label >{{__("Last Name")}} <span class="required">*</span></label>
                    <input type="text" placeholder="{{__("Last Name")}}" class="form-control" value="{{$user->last_name ?? ''}}" name="last_name">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label >{{__("Phone")}} <span class="required">*</span></label>
                    <input type="phone" placeholder="{{__("Your Phone")}}" class="form-control" value="{{$user->phone ?? ''}}" name="phone">
                </div>
            </div>

            <div class="col-md-6 field-country">
                <div class="form-group">
                    <label >{{__("Country")}} <span class="required">*</span> </label>
                    <select name="country" class="form-control country">
                        <option value="">{{__('-- Select --')}}</option>
                        @foreach(get_country_lists() as $id=>$name)
                            <option @if(($user->country ?? '') == $id) selected @endif value="{{$id}}">{{$name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            {{--  <div class="col-md-12">
                <div class="form-group">
                    <label>{{__("Date Of Birth")}}</label>
                    <input type="date" required name="dob" value="{{ $user->birthday ?? '' }}" max=" \Carbon\Carbon::now()->toDateString() }}" placeholder="{{__("Date Of Birth")}}" class="form-control">
                </div>
            </div>  --}}

            <button class="btn btn-primary btn-add-passenger">{{__('Add Passenger')}}
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    @include ('Booking::frontend/booking/booking-guests-info', ['display' => 0])
    {{--  @include ($service->checkout_form_payment_file ?? 'Booking::frontend/booking/checkout-payment')  --}}

    @php
    $term_conditions = setting_item('booking_term_conditions');
    @endphp

    <div class="form-group">
        <label class="term-conditions-checkbox">
            <input type="checkbox" name="term_conditions"> {{__('I have read and accept the')}}  <a target="_blank" href="{{get_page_url($term_conditions)}}">{{__('terms and conditions')}}</a>
        </label>
    </div>
    @if(setting_item("booking_enable_recaptcha"))
        <div class="form-group">
            {{recaptcha_field('booking')}}
        </div>
    @endif
    <div class="html_before_actions"></div>

    <p class="alert-text mt10" v-show=" message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></p>

    <div class="form-actions">
        <button class="btn btn-danger" @click="doCheckout">{{__('Submit')}}
            <i class="fa fa-spin fa-spinner" v-show="onSubmit"></i>
        </button>
    </div>
</div>
@include('Booking::frontend/booking/passenger-modal')


