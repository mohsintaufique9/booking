<div class="modal fade" id="modal-add-passenger">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{__("Add Passenger")}}: #{{$booking->id}}</h4>
            </div>
            <form action="{{ route('booking.add-passenger', $booking->code)}}" class="form" id="add-passenger-form">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label >{{__("ID")}} <span class="required">*</span> </label>
                                <select required name="id_type" class="form-control id_type_pass">
                                    <option value="">{{__('-- Select --')}}</option>
                                    <option value="0">Tanzanian ID</option>
                                    <option value="1">Passport</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label >{{__("ID Number")}} <span class="required">*</span></label>
                                <input type="text" minlength="20" maxlength="20" required placeholder="{{__("Enter ID Number")}}" max="20" class="id-number-pass form-control" name="id_number">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("Expiration Date")}}</label>
                                <input type="date" requied name="expiration_date" placeholder="{{__("Date Of Birth")}}" min="{{ \Carbon\Carbon::now()->toDateString()}}" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-6 field-country">
                            <div class="form-group">
                                <label >{{__("Country")}} <span class="required">*</span> </label>
                                <select name="country" required class="form-control country_pass">
                                    <option value="">{{__('-- Select --')}}</option>
                                    @foreach(get_country_lists() as $id=>$name)
                                        <option @if(($user->country ?? '') == $id) selected @endif value="{{$id}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label >{{__("First Name")}} <span class="required">*</span></label>
                                <input type="text" required maxlength="50" placeholder="{{__("First Name")}}" class="form-control" name="f_name">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label >{{__("Last Name")}} <span class="required">*</span></label>
                                <input type="text" required maxlength="50" placeholder="{{__("Last Name")}}" class="form-control" name="l_name">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label >{{__("Gender")}} <span class="required">*</span> </label>
                                <select required name="gender" class="form-control">
                                    <option value="">{{__('-- Select --')}}</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                        @if($booking->object_model == 'flight')
                        <div class="col-md-6">
                            <div class="form-group">
                                <label >{{__("Ticket Type")}} <span class="required">*</span> </label>
                                <select required name="ticket_type" class="form-control">
                                    <option value="">{{__('-- Select --')}}</option>
                                    <option value="1">Adult</option>
                                    <option value="0">Child</option>
                                </select>
                            </div>
                        </div>
                        @endif

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("Date Of Birth")}}</label>
                                <input type="date" required name="dob" max="{{ \Carbon\Carbon::now()->toDateString()}}" placeholder="{{__("Date Of Birth")}}" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <span class="btn btn-secondary" data-dismiss="modal">{{__("Close")}}</span>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
