<div class="booking-review">
    <h4 class="booking-review-title">{{__('Your Information')}}</h4>
    <div class="booking-review-content">
        <div class="review-section">
            <div class="info-form">
                <ul>
                    <li class="info-first-name">
                        <div class="label">{{__('First name')}}</div>
                        <div class="val">{{$booking->first_name}}</div>
                    </li>
                    <li class="info-last-name">
                        <div class="label">{{__('Last name')}}</div>
                        <div class="val">{{$booking->last_name}}</div>
                    </li>
                    <li class="info-email">
                        <div class="label">{{__('Email')}}</div>
                        <div class="val">{{$booking->email}}</div>
                    </li>
                    <li class="info-phone">
                        <div class="label">{{__('Phone')}}</div>
                        <div class="val">{{$booking->phone}}</div>
                    </li>
                    <li class="info-address">
                        <div class="label">{{__('ID Type')}}</div>
                        <div class="val">{{$booking->getIDType()}}</div>
                    </li>
                      <li class="info-address">
                        <div class="label">{{__('ID Number')}}</div>
                        <div class="val">{{$booking->id_number}}</div>
                    </li>
    
                    <li class="info-country">
                        <div class="label">{{__('Country')}}</div>
                        <div class="val">{{get_country_name($booking->country)}}</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

@include ('Booking::frontend/booking/booking-guests-info', ['display' => 1])
