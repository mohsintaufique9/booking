<?php
namespace Modules\Booking\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;
use Modules\Booking\Emails\NewBookingEmail;
use Modules\Booking\Emails\StatusUpdatedEmail;
use Modules\Booking\Events\BookingUpdatedEvent;
use Modules\Tour\Models\Tour;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingGuest extends Model
{

    protected $table = 'bravo_booking_guests';

    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id', 'id');
    }

    public function getIDType() {
        if($this->id_type == 0) {
            return 'Tanzanian National ID';
        } else {
            return 'Passport';
        }
    }

    public function getType() {
        if($this->is_adult == 0) {
            return 'Child';
        } else {
            return 'Adult';
        }
    }
}