<?php
namespace Modules\Booking\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Mockery\Exception;
//use Modules\Booking\Events\VendorLogPayment;
use Modules\Tour\Models\TourDate;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Booking\Models\Booking;
use Modules\Booking\Models\BookingGuest;
use App\Helpers\ReCaptchaEngine;

class BookingController extends \App\Http\Controllers\Controller
{
    use AuthorizesRequests;
    protected $booking;

    public function __construct()
    {
        $this->booking = Booking::class;
    }

    public function checkout($code)
    {

        $booking = $this->booking::where('code', $code)->first();

        if (empty($booking)) {
            abort(404);
        }
        if ($booking->customer_id != Auth::id()) {
            abort(404);
        }

        if($booking->status != 'draft'){
            return redirect('/');
        }
        $data = [
            'page_title' => __('Checkout'),
            'booking'    => $booking,
            'service'    => $booking->service,
            'gateways'   => $this->getGateways(),
            'user'       => Auth::user()
        ];
        return view('Booking::frontend/checkout', $data);
    }

    public function checkStatusCheckout($code)
    {
        $booking = $this->booking::where('code', $code)->first();
        $data = [
            'error'    => false,
            'message'  => '',
            'redirect' => ''
        ];
        if (empty($booking)) {
            $data = [
                'error'    => true,
                'redirect' => url('/')
            ];
        }
        if ($booking->customer_id != Auth::id()) {
            $data = [
                'error'    => true,
                'redirect' => url('/')
            ];
        }
        if ($booking->status != 'draft') {
            $data = [
                'error'    => true,
                'redirect' => url('/')
            ];
        }
        return response()->json($data, 200);
    }

    public function doCheckout(Request $request)
    {

        /**
         * @param Booking $booking
         */
        $validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);
        if ($validator->fails()) {
            $this->sendError('', ['errors' => $validator->errors()]);
        }
        $code = $request->input('code');
        $booking = $this->booking::where('code', $code)->first();
        if (empty($booking)) {
            abort(404);
        }
        if ($booking->customer_id != Auth::id()) {
            abort(404);
        }
        if ($booking->status != 'draft') {
            return $this->sendError('',[
                'url'=>$booking->getDetailUrl()
            ]);
        }
        $service = $booking->service;
        if (empty($service)) {
            $this->sendError(__("Service not found"));
        }
        /**
         * Google ReCapcha
         */
        if(ReCaptchaEngine::isEnable() and setting_item("booking_enable_recaptcha")){
            $codeCapcha = $request->input('g-recaptcha-response');
            if(!$codeCapcha or !ReCaptchaEngine::verify($codeCapcha)){
                $this->sendError(__("Please verify the captcha"));
            }
        }
        $rules = [
            'first_name'      => 'required|string|max:255',
            'last_name'       => 'required|string|max:255',
            'id'       => 'required',
            'id_num'     => 'required',
            'email'           => 'required|string|email|max:255',
            'phone'           => 'required|string|max:255',
            'country' => 'required',
            // 'payment_gateway' => 'required',
            'term_conditions' => 'required'
        ];
        $rules = $service->filterCheckoutValidate($request, $rules);
        if (!empty($rules)) {
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $this->sendError('', ['errors' => $validator->errors()]);
            }
        }
        // if (!empty($rules['payment_gateway'])) {
        //     $payment_gateway = $request->input('payment_gateway');
        //     $gateways = get_payment_gateways();
        //     if (empty($gateways[$payment_gateway]) or !class_exists($gateways[$payment_gateway])) {
        //         $this->sendError(__("Payment gateway not found"));
        //     }
        //     $gatewayObj = new $gateways[$payment_gateway]($payment_gateway);
        //     if (!$gatewayObj->isAvailable()) {
        //         $this->sendError(__("Payment gateway is not available"));
        //     }
        // }
        //Checks if rooms are available 
        $service->beforeCheckout($request, $booking);
        //Normal Checkout
        $booking->first_name = $request->input('first_name');
        $booking->last_name = $request->input('last_name');
        $booking->email = $request->input('email');
        $booking->id_type = $request->input('id');
        $booking->id_number = $request->input('id_num');
        $booking->expiration_date = $request->input('exp_date');
        $booking->phone = $request->input('phone');
        $booking->country = $request->input('country');
        $booking->status = 'confirmed';
        $booking->save();

    //    event(new VendorLogPayment($booking));

        $user = Auth::user();
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->phone = $request->input('phone');
        $user->id_type = $request->input('id');
        $user->id_number = $request->input('id_num');
        $user->expiration_date = $request->input('exp_date');
        $user->save();

        $booking->addMeta('locale',app()->getLocale());

        $service->afterCheckout($request, $booking);

        return response()->json([
            'url' => $booking->getDetailUrl()
        ]);

        // try {

        //     $gatewayObj->process($request, $booking, $service);
        // } catch (Exception $exception) {
        //     $this->sendError($exception->getMessage());
        // }
    }

    public function confirmPayment(Request $request, $gateway)
    {

        $gateways = get_payment_gateways();
        if (empty($gateways[$gateway]) or !class_exists($gateways[$gateway])) {
            $this->sendError(__("Payment gateway not found"));
        }
        $gatewayObj = new $gateways[$gateway]($gateway);
        if (!$gatewayObj->isAvailable()) {
            $this->sendError(__("Payment gateway is not available"));
        }
        return $gatewayObj->confirmPayment($request);
    }

    public function cancelPayment(Request $request, $gateway)
    {

        $gateways = get_payment_gateways();
        if (empty($gateways[$gateway]) or !class_exists($gateways[$gateway])) {
            $this->sendError(__("Payment gateway not found"));
        }
        $gatewayObj = new $gateways[$gateway]($gateway);
        if (!$gatewayObj->isAvailable()) {
            $this->sendError(__("Payment gateway is not available"));
        }
        return $gatewayObj->cancelPayment($request);
    }

    /**
     * @todo Handle Add To Cart Validate
     *
     * @param Request $request
     * @return string json
     */
    public function addToCart(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'service_id'   => 'required|integer',
            'service_type' => 'required'
        ]);
        if ($validator->fails()) {
            $this->sendError('', ['errors' => $validator->errors()]);
        }
        $service_type = $request->input('service_type');
        $service_id = $request->input('service_id');
        $allServices = get_bookable_services();
        if (empty($allServices[$service_type])) {
            $this->sendError(__('Service type not found'));
        }
        $module = $allServices[$service_type];
        $service = $module::find($service_id);
        if (empty($service) or !is_subclass_of($service, '\\Modules\\Booking\\Models\\Bookable')) {
            $this->sendError(__('Service not found'));
        }
        if (!$service->isBookable()) {
            $this->sendError(__('Service is not bookable'));
        }
        //        try{\
        $service->addToCart($request);
        //
        //        }catch(\Exception $ex){
        //            $this->sendError($ex->getMessage(),['code'=>$ex->getCode()]);
        //        }
    }

    protected function getGateways()
    {

        $all = get_payment_gateways();
        $res = [];
        foreach ($all as $k => $item) {
            if (class_exists($item)) {
                $obj = new $item($k);
                if ($obj->isAvailable()) {
                    $res[$k] = $obj;
                }
            }
        }
        return $res;
    }

    public function detail(Request $request, $code)
    {

        $booking = Booking::where('code', $code)->first();
        if (empty($booking)) {
            abort(404);
        }

        if ($booking->status == 'draft') {
            return redirect($booking->getCheckoutUrl());
        }
        if ($booking->customer_id != Auth::id()) {
            abort(404);
        }
        $data = [
            'page_title' => __('Booking Details'),
            'booking'    => $booking,
            'service'    => $booking->service,
        ];
        if ($booking->gateway) {
            $data['gateway'] = get_payment_gateway_obj($booking->gateway);
        }
        return view('Booking::frontend/detail', $data);
    }

    public function addPassenger(Request $request, $code) {

        $booking = Booking::where('code', $code)->first();
        if (empty($booking)) {
            abort(404);
        }

        if($request->has('ticket_type')) {

            $class = $request->ticket_type;

            if($class == 1) {
                $booking->adults = $booking->adults + 1;
                $booking->total = $booking->total + $booking->getBookingTotal(0)->price;
                $booking->total_before_fees = $booking->total_before_fees + $booking->getBookingTotal(0)->price;
            } else if ($class == 0) {
                $booking->child = $booking->child + 1;
                $booking->total = $booking->total + $booking->getBookingTotal(1)->price;
                $booking->total_before_fees = $booking->total_before_fees + $booking->getBookingTotal(1)->price;
            }

        } else {
    
            if($booking->sc_number > 0) {
                $booking->sc_number = $booking->sc_number + 1;
                $booking->total = $booking->total + ($booking->service->getscPrice());
                $booking->total_before_fees = $booking->total_before_fees + $booking->service->getscPrice();
            }
    
            if($booking->bc_number > 0) {
                $booking->bc_number = $booking->bc_number + 1;
                $booking->total = $booking->total + ($booking->service->getbcPrice());
                $booking->total_before_fees = $booking->total_before_fees + $booking->service->getbcPrice();
            }
    
            if($booking->ec_number > 0) {
                $booking->ec_number = $booking->ec_number + 1;
                $booking->total = $booking->total + ($booking->service->getecPrice());
                $booking->total_before_fees = $booking->total_before_fees + $booking->service->getecPrice();
            }

            if($booking->fc_number > 0) {
                $booking->fc_number = $booking->fc_number + 1;
                $booking->total = $booking->total + ($booking->service->getfcPrice());
                $booking->total_before_fees = $booking->total_before_fees + $booking->service->getfcPrice();
            }
        }

        $booking->save();

        $booking_guest = new BookingGuest();
        $booking_guest->booking_id = $booking->id;
        $booking_guest->first_name = $request->f_name;
        $booking_guest->last_name = $request->l_name;
        $booking_guest->id_type = $request->id_type;
        $booking_guest->id_number = $request->id_number;
        $booking_guest->gender = $request->gender;
        $booking_guest->country = $request->country ?? 'TZ';

        if($request->has('ticket_type')) {
            $booking_guest->is_adult = $request->ticket_type;
        }

        $booking_guest->expiration_date = $request->expiration_date;
        $booking_guest->dob = $request->dob;

        $booking_guest->save();

        return redirect()->route('booking.checkout', $booking->code);
    }

    public function removePassenger($id) {

        $passenger = BookingGuest::findOrFail($id);

        $booking = Booking::findOrFail($passenger->booking->id);
        if (empty($booking)) {
            abort(404);
        }

        if($booking->fc_number > 1) {
            $booking->fc_number = $booking->fc_number - 1;
            $booking->total = $booking->total - ($booking->service->getfcPrice());
            $booking->total_before_fees = $booking->total_before_fees - $booking->service->getfcPrice();
        }

        if($booking->sc_number > 1) {
            $booking->sc_number = $booking->sc_number - 1;
            $booking->total = $booking->total - ($booking->service->getscPrice());
            $booking->total_before_fees = $booking->total_before_fees - $booking->service->getscPrice();
        }

        if($booking->bc_number > 1) {
            $booking->bc_number = $booking->bc_number - 1;
            $booking->total = $booking->total - ($booking->service->getbcPrice());
            $booking->total_before_fees = $booking->total_before_fees - $booking->service->getbcPrice();
        }

        if($booking->ec_number > 1) {
            $booking->ec_number = $booking->ec_number - 1;
            $booking->total = $booking->total - ($booking->service->getecPrice());
            $booking->total_before_fees = $booking->total_before_fees - $booking->service->getecPrice();
        }

        if($passenger->is_adult == 1) {
            $booking->adults = $booking->adults - 1;
            $booking->total = $booking->total - $booking->getBookingTotal(0)->price;
            $booking->total_before_fees = $booking->total_before_fees - $booking->getBookingTotal(0)->price;
        }

        if($passenger->is_adult == 0) {
            $booking->child = $booking->child - 1;
            $booking->total = $booking->total - $booking->getBookingTotal(1)->price;
            $booking->total_before_fees = $booking->total_before_fees -  $booking->getBookingTotal(1)->price;
        }

        $booking->save();

        $passenger->delete();

        return redirect()->route('booking.checkout', $booking->code);
    }

    public function payment(Request $request, $code) {

        $this->validate($request, [
            'payment_gateway' => 'required',
        ]);

        $booking = $this->booking::where('code', $code)->first();
        if (empty($booking)) {
            abort(404);
        }

        $payment_gateway = $request->input('payment_gateway');
        $gateways = get_payment_gateways();
        if (empty($gateways[$payment_gateway]) or !class_exists($gateways[$payment_gateway])) {
            $this->sendError(__("Payment gateway not found"));
        }
        $gatewayObj = new $gateways[$payment_gateway]($payment_gateway);

        if (!$gatewayObj->isAvailable()) {
            $this->sendError(__("Payment gateway is not available"));
        }

         try {

            $response = $gatewayObj->process($request, $booking, $booking->service);
            return redirect()->to($response->getData()->url);
        } catch (Exception $exception) {
            $this->sendError($exception->getMessage());
        }
    }
}
