<?php
use Illuminate\Support\Facades\Route;
// Booking
Route::group(['prefix'=>config('booking.booking_route_prefix')],function(){
    Route::post('/addToCart','BookingController@addToCart')->middleware('auth');
    Route::post('/doCheckout','BookingController@doCheckout')->middleware('auth');
    Route::get('/confirm/{gateway}','BookingController@confirmPayment');
    Route::get('/cancel/{gateway}','BookingController@cancelPayment');
    Route::get('/{code}','BookingController@detail')->middleware('auth');
    Route::get('/{code}/checkout','BookingController@checkout')->middleware('auth')->name('booking.checkout');;
    Route::get('/{code}/add-passenger','BookingController@addPassenger')->middleware('auth')->name('booking.add-passenger');
    Route::get('/{id}/remove-passenger','BookingController@removePassenger')->middleware('auth')->name('booking.remove-passenger');
    Route::post('/{code}/payment','BookingController@payment')->middleware('auth')->name('booking.payment');
    Route::get('/{code}/check-status','BookingController@checkStatusCheckout')->middleware('auth');
});
