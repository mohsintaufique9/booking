<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryName extends Model
{
    protected $table = 'country_name';
}
