<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';

    public function country_code()
    {
        return $this->hasOne(Country::class, 'code2l', 'code2l');
    }
}
